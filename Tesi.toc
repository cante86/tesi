\select@language {italian}
\contentsline {section}{\numberline {1}Introduzione sulla crittografia}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Significato}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Evoluzione}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Scopo}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}Come funziona il protocollo SSL}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Secure Socket Layer}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Livello di applicazione}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Livello di trasporto}{8}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}TCP}{8}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}UDP}{10}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}Come \IeC {\`e} gestito il flusso di dati tramite SSL}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Regole dei messaggi}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Scambio di messaggi}{11}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Requisiti per lo scambio}{11}{subsection.3.3}
\contentsline {section}{\numberline {4}Certificati SSL}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}Introduzione}{12}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Caratteristiche}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Certificate authority}{13}{subsection.4.3}
\contentsline {section}{\numberline {5}Infrastrutture crittografiche}{13}{section.5}
\contentsline {subsection}{\numberline {5.1}Chiave privata}{13}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Chiave pubblica}{15}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Combinazione di chiave pubblica e privata}{17}{subsection.5.3}
\contentsline {section}{\numberline {6}Implementazione in JavaRMI}{17}{section.6}
\contentsline {subsection}{\numberline {6.1}Sockets}{17}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Codice di esempio}{19}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Generazione delle chiavi}{20}{subsection.6.3}
\contentsline {section}{\numberline {7}Conclusioni}{22}{section.7}
